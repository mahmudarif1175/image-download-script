const fs = require("fs");
const store = require('data-store')({ path: process.cwd() + '/data/projects.json' });
const parse = require("csv-parse/lib/sync");
const request = require("request");
const download = require("image-downloader");

const contents = fs.readFileSync("./product-link.csv", "utf-8");
const data = parse(contents, { columns: true });
const total = data.length;
console.log(total);

async function downloadIMG() {
  for (let i = 1; i < total; i++) {
    const options = {
      url: data[i].Link,
      dest: "./images" // Save to /path/to/dest/image.jpg
    };
    try {
      const { filename, image } = await download.image(options);
      console.log(i, filename); // => /path/to/dest/image.jpg
    } catch (e) {
      console.error(e);
      store.union('url', data[i].Link);

    }
  }
}
downloadIMG();
